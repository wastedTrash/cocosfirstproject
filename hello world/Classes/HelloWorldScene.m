//
//  HelloWorldScene.m
//
//  Created by : skriganovsv
//  Project    : hello world
//  Date       : 01.08.16
//
//  Copyright (c) 2016 skriganovsv.
//  All rights reserved.
//
// -----------------------------------------------------------------

#import "HelloWorldScene.h"
#import "../Libraries/cocos2d/CCParticleExamples.h"

// -----------------------------------------------------------------------
@interface HelloWorldScene()

@property (nonatomic, strong) CCSprite9Slice *background;

@property (nonatomic, strong) NSMutableArray *boysArray;

@end

@implementation HelloWorldScene

// -----------------------------------------------------------------------

- (id)init{
    // Apple recommend assigning self with supers return value
    self = [super init];
    
    //Enable touches on screen
    self.userInteractionEnabled = TRUE;
    
    // The thing is, that if this fails, your app will 99.99% crash anyways, so why bother
    // Just make an assert, so that you can catch it in debug
    NSAssert(self, @"Whoops");
    
    // Background
    _background = [CCSprite9Slice spriteWithImageNamed:@"white_square.png"];
    _background.anchorPoint = CGPointZero;
    _background.contentSize = [CCDirector sharedDirector].viewSize;
    _background.color = [CCColor grayColor];
    _background.color = [CCColor blackColor];
    [self addChild:_background];
    
    // The standard Hello World text
    CCLabelTTF *label = [CCLabelTTF labelWithString:@"Hello World" fontName:@"Times New Roman" fontSize:64];
    label.positionType = CCPositionTypeNormalized;
    label.position = (CGPoint){0.5, 0.5};
    label.fontColor = [CCColor colorWithRed:1.0f green:0.5f blue:0.4f];
    
    [self addChild:label];
    
    [self schedule:@selector(animateBackground) interval:1];
    
    _boysArray = [NSMutableArray array];
    
    
    // done
    return self;
}


- (void)animateBackground {
    _background.color = [CCColor colorWithRed:(float)rand()/RAND_MAX
                                        green:(float)rand()/RAND_MAX
                                         blue:(float)rand()/RAND_MAX ];
    if (([_boysArray count] != 0) && (self.children.count > 2)){
        CCSprite *temp = [_boysArray firstObject];
        if (temp.position.y  < 10){
            [self removeChild:temp cleanup:TRUE];
            [_boysArray removeObjectAtIndex:0];
            
        }
    }
    
}

- (void)touchBegan:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    CGPoint touchLocation = [touch locationInNode:self];
    
    NSString *imageName = [NSString stringWithFormat:@"white_square.png"];
    UIImage *image = [UIImage imageNamed:imageName];
    assert(image);
    
    
    CCSprite *happyBoy = [CCSprite spriteWithImageNamed:@"HappyBoy.png"];
    happyBoy.scale = 0.10f;
    happyBoy.position = touchLocation;
    [_boysArray addObject:(happyBoy)];
    [self addChild:_boysArray.lastObject];
    
    CCParticleFire *fire = [[CCParticleFire alloc] init];
    fire.scale = 10;
    fire.position = (CGPoint){happyBoy.contentSize.width/2, happyBoy.contentSize.height/2};
    [happyBoy addChild:fire];
    
}

- (void)touchMoved:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    CGPoint touchLocation = [touch locationInNode:self];
    CCSprite *temp = [_boysArray lastObject];
    
    temp.position = touchLocation;
}

- (void)touchEnded:(CCTouch *)touch withEvent:(CCTouchEvent *)event{
    CCSprite *temp = [_boysArray lastObject];
    CCAction *falling = [CCActionMoveTo actionWithDuration:3 position:CGPointMake(temp.position.x, 0)];
    [temp runAction:falling];
}



// -----------------------------------------------------------------------

@end























// why not add a few extra lines, so we dont have to sit and edit at the bottom of the screen ...
